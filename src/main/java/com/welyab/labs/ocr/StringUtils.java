package com.welyab.labs.ocr;

public class StringUtils {

	public static boolean isEmpty(String fileName) {
		return fileName == null || fileName.isEmpty();
	}

	public static boolean isNotEmpty(String parameter) {
		return !isEmpty(parameter);
	}
}
