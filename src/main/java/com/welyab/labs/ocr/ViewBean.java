package com.welyab.labs.ocr;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@Named("vb")
@RequestScoped
public class ViewBean {

	private List<Path> files;

	@PostConstruct
	public void init() throws IOException {
		loadFiles();
	}

	private void loadFiles() throws IOException {
		files = Files.list(Utils.getFileStore())
				.filter(p -> !p.getFileName().endsWith(".ocr"))
				.map(p -> p.getFileName())
				.collect(Collectors.toList());

		Path localImagesDir = Paths
				.get(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/tests"));
		Files.list(localImagesDir)
				.filter(p -> !p.toFile().getName().endsWith(".ocr"))
				.forEach(localImage -> {
					try {
						Path targetImage = Utils.getFileStore().resolve(localImage.getFileName());
						FileChannel localChannel = FileChannel.open(localImage);
						FileOutputStream fileOutputStream = new FileOutputStream(targetImage.toFile());
						FileChannel targetChannel = fileOutputStream.getChannel();
						localChannel.transferTo(0, Files.size(localImage), targetChannel);
						localChannel.close();
						targetChannel.close();
						fileOutputStream.close();
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				});

		files = Files.list(Utils.getFileStore())
				.filter(p -> !p.toFile().getName().endsWith(".ocr"))
				.map(p -> p.getFileName())
				.collect(Collectors.toList());
	}

	public void showText(String file) {
		Map<String, Object> configuration = new HashMap<>();
		configuration.put("modal", true);
		configuration.put("width", 800);
		configuration.put("height", 300);

		Map<String, List<String>> parameters = new HashMap<>();
		List<String> arrayList = new ArrayList<>();
		arrayList.add(file);
		parameters.put("file", arrayList);

		RequestContext.getCurrentInstance().openDialog("/pages/show-text.xhtml", configuration, parameters);
	}

	public void onUpload(FileUploadEvent event) throws FileNotFoundException, IOException {
		UploadedFile uploadedFile = event.getFile();
		Path fileName = Paths.get(UUID.randomUUID().toString() + "." + uploadedFile.getFileName()).getFileName();
		FileOutputStream fileOutputStream = new FileOutputStream(Utils.getFileStore().resolve(fileName).toFile());
		FileChannel outChannel = fileOutputStream.getChannel();
		ReadableByteChannel inChannel = Channels.newChannel(uploadedFile.getInputstream());
		outChannel.transferFrom(inChannel, 0, uploadedFile.getSize());
		outChannel.close();
		inChannel.close();
		fileOutputStream.close();
		loadFiles();
	}

	public List<Path> getFiles() {
		return files;
	}
}
