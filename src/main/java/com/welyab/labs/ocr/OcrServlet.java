package com.welyab.labs.ocr;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asprise.ocr.Ocr;

@WebServlet("/read-chars")
@SuppressWarnings("javadoc")
public class OcrServlet extends HttpServlet {

	static {
		Ocr.setUp();
	}

	private static final long serialVersionUID = 1L;

	private static final String FILE_PARAMETER = "file";

	private static final String CACHE_PARAMETER = "cache";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String fileName = req.getParameter(FILE_PARAMETER);
		if (StringUtils.isEmpty(fileName)) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND, "ocr-test");
			return;
		}

		Path file = Utils.getFileStore().resolve(fileName);
		if (!Files.exists(file)) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND, "ocr-test");
			return;
		}

		if (StringUtils.isNotEmpty(req.getParameter(CACHE_PARAMETER))
				&& req.getParameter(CACHE_PARAMETER).equals("true")) {
			Path ocr = file.getParent().resolve(Paths.get(file.getFileName() + ".ocr"));
			if (Files.exists(ocr)) {
				ServletOutputStream outputStream = resp.getOutputStream();
				WritableByteChannel outChannel = Channels.newChannel(outputStream);
				FileChannel fileChannel = FileChannel.open(ocr);
				fileChannel.transferTo(0, Files.size(ocr), outChannel);
				fileChannel.close();
				outChannel.close();
				return;
			}
		}

		File files[] = {
				file.toFile()
		};

		Ocr ocr = new Ocr();
		ocr.startEngine("por", Ocr.SPEED_SLOW);
		String text = ocr.recognize(files, Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);

		resp.getWriter().write(text);

		Path ocrCache = file.getParent().resolve(Paths.get(file.getFileName() + ".ocr"));
		BufferedWriter writer = Files.newBufferedWriter(ocrCache);
		writer.write(text);
		writer.flush();
		writer.close();
	}
}
