package com.welyab.labs.ocr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Utils {

	public static Path getFileStore() throws IOException {
		Path path = Paths.get(System.getProperty("user.home")).resolve("ocr/files");
		Files.createDirectories(path);
		return path;
	}
}
