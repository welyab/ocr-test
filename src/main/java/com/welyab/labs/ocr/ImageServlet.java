package com.welyab.labs.ocr;

import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/img")
public class ImageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Path path = Utils.getFileStore().resolve(req.getParameter("file"));
		FileChannel inChannel = FileChannel.open(path);
		WritableByteChannel outChannel = Channels.newChannel(resp.getOutputStream());
		inChannel.transferTo(0, Files.size(path), outChannel);
		inChannel.close();
		outChannel.close();
	}
}
